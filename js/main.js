app = {};

app.slideMenuOut = function (e) {
  if (e) {
    e.preventDefault();
  }
  $('div.tag').off();
  $('div.controls').animate(
      {left: 0},
      500,
      'swing',
      function (e) {
        $('div.tag').on('click', app.slideMenuIn);
      }
  );
};

app.slideMenuIn = function (e) {
  if (e) {
    e.preventDefault();
  }
  $('div.tag').off();
  $('div.controls').animate(
      {left: -250},
      500,
      'swing',
      function () {
        $('div.tag').on('click', app.slideMenuOut);
      }
  );
};

app.displayFrontData = function (type) {
  return function () {
    $('div.tag').off();
    app.slideMenuIn();
    $('div.data').html($('#' + type + '-template').text())
    $('div#front')
      .addClass(type)
      .animate(
        {top: "50%"},
        500,
        "swing"
      );
    $('div.tag').on('click', app.retractFront);
  }
}

app.retractFront = function () {
  $('div.tag').off();
  $('div#front')
    .animate(
      {top: "150%"},
      600,
      "swing"
    )
    .attr( "class", "");
  app.slideMenuOut();
}

preload = function (arrayOfImages) {
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
    });
}

preload([ 'img/xo.png', 'img/askterix.png', 'img/coffee-run.png', 'img/meta-base.png' ]);

$(document).ready( function () {
  $('div.tag').on('click', app.slideMenuOut);
  $('#about').on('click', app.displayFrontData("about"));
  $('#skills').on('click', app.displayFrontData("skills"));
  $('#projects').on('click', app.displayFrontData("projects"));
  $('#contact').on('click', app.displayFrontData("contact"));
  $('#spiral').on('click', app.displayFrontData("spiral"));
  $('div#close').on('click', app.retractFront);
});
